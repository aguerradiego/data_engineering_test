from mock import patch
from pyspark.shell import spark

from the_cocktail_api.cocktail_app import get_user_buys, get_monthly_average, get_monthly_percentage_change
from the_cocktail_api.write_data import check_json_schema


@patch('the_cocktail_api.cocktail_app._get_user_buys')
def test_get_user_buys(mock_get_user_buys):
    test_data = [{"user_id": 1, "date": "2020-2-02", "product_name": "P3", "price": 8.0, "purchased": "true"}]
    mock_get_user_buys.return_value = spark.createDataFrame(data=test_data)

    assert get_user_buys(1) == '[{"date":"2020-2-02","price":8.0,"product_name":"P3","purchased":"true","user_id":1}]'


@patch('the_cocktail_api.cocktail_app._get_monthly_average')
def test_get_monthly_average(mock_get_monthly_average):
    test_data = [[10]]
    mock_get_monthly_average.return_value = spark.createDataFrame(data=test_data)

    assert get_monthly_average(1) == "[[10]]"


@patch('the_cocktail_api.cocktail_app._get_monthly_percentage_change')
def test_get_monthly_percentage_change(mock_get_monthly_percentage_change):
    test_data = [["P3",50.0,-66.6666666667,200.0,-66.6666666667,200.0,33.3333333333,-50.0,50.0,-33.3333333333,-50.0,200.0]]
    mock_get_monthly_percentage_change.return_value = spark.createDataFrame(data=test_data)

    assert get_monthly_percentage_change(2020) == '[["P3",50.0,-66.6666666667,200.0,-66.6666666667,200.0,33.3333333333,-50.0,50.0,-33.3333333333,-50.0,200.0]]'


def test_check_json_schema():
    content = {"id": 1, "product": "ABC"}
    schema = {"type": "object",
              "properties": {
                  "id": {"type": "integer"},
                  "product": {"type": "string"}
              }}

    assert check_json_schema(content, schema), True



