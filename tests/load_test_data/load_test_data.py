import requests
import json


url = "http://127.0.0.1:5000/api/add_item/"
headers = {'Content-type': 'application/json'}
test_data = json.load(open('test_data.json'))

for i in test_data['test_data']:
    r = requests.post(url, data=json.dumps(i), headers=headers)
    print(i)
    print(r.content)
