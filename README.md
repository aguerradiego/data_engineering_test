# Prueba técnica Data Engineering
La tarea ha sido resuelta utilizando Python 3 como lenguaje, Spark como framework para el tratamiento datos y Flask para
desarrollar el endpoint. 

Spark, ha sido elegido con el objetivo de cubrir el requisito de poder trabajar con gran cantidad de datos.
Además, los datos se guardan en formato Parquet, ganando velocidad a la hora de consultarlos además de optimizar el espacio comparado con texto plano.

Flask, ha sido elegido por su simplicidad y capacidad para desarrollar aplicaciones webs simples de manera fácil y rápida.

El proyecto está divido en dos módulos independientes, el primero se encarga de comprobar y guardar los datos en local y el segundo
se encarga de extraer las métricas pedidas en la tarea.

## Instalación

1. Clona este proyecto
2. Accede a la carpeta del proyecto `cd data_engineering_test`
3. Instala las dependencias `pip install -r requirements.txt`
4. Instala Spark https://spark.apache.org/downloads.html

## Uso

### Arranca la aplicación:

1. Accede a la carpeta del proyecto `cd data_engineering_test`
2. Ejecuta la aplicación `python the_cocktail_api/cocktail_app.py`

### Carga algunos datos de prueba:

1. Accede a la carpeta del proyecto `cd data_engineering_test`
2. Ejecuta la carga de datos `python tests/load_test_data/load_test_data.py`

### Uso de la API:

Guarda un nuevo registro:
`curl -X POST http://127.0.0.1:5000/api/add_item/ -H 'content-type: application/json' -d '{"user_id":10,"date":"2018-06-02","product_name":"P3","price":1000,"purchased": true}'`

Para consultar las métricas, desde el navegador o un cliente como postman, accede a las siguientes URL.

- **Consulta la variación del tráfico por producto (ejemplo, año 2020):**  <http://127.0.0.1:5000/api/monthly_change/year/2020>
- **Consulta la media de ventas mensual de un año (ejemplo, año 2020):**  <http://127.0.0.1:5000/api/monthly_average/year/2020>
- **Consulta las compras de un usuario (ejemplo, usuario 1):**  <http://127.0.0.1:5000/api/user_buys/user/1/>

## Tests

Para ejecutar los test unitarios utilizaremos pytest del siguiente modo: 

1. Accede a la carpeta del proyecto `cd data_engineering_test`
2. Ejecuta la carga de datos `pytest tests/tests.py`


