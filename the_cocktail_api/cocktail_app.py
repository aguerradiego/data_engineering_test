import flask
from flask import request

from get_metrics import _get_user_buys, _get_monthly_average, _get_monthly_percentage_change
from write_data import check_json_schema, write_item_to_parquet, schema

app = flask.Flask(__name__)


@app.route('/api/add_item/', methods=['POST'])
def add_item():
    if request.is_json:
        content = request.get_json()
        if check_json_schema(content, schema):
            write_item_to_parquet(content)
            return 'POST OK'
        else:
            return 'POST FAIL'


@app.route('/api/user_buys/user/<string:user_id>/', methods=['GET'])
def get_user_buys(user_id):
    buys = _get_user_buys(user_id)
    return buys.toPandas().to_json(orient='records')


@app.route('/api/monthly_average/year/<int:year>/', methods=['GET'])
def get_monthly_average(year):
    average = _get_monthly_average(year)
    return average.toPandas().to_json(orient='values')


@app.route('/api/monthly_change/year/<int:year>/', methods=['GET'])
def get_monthly_percentage_change(year):
    change = _get_monthly_percentage_change(year)
    return change.toPandas().to_json(orient='values')


app.run()
