from pyspark.shell import spark


def _get_user_buys(user_id: int):
    items_file = spark.read.parquet("output/items.parquet")
    items_file.createOrReplaceTempView("items_file")

    user_buys = spark.sql(
        """
        SELECT * 
        FROM items_file 
        WHERE user_id = {0}
        """.format(user_id))

    return user_buys


def _get_monthly_average(year: int):
    items_file = spark.read.parquet("output/items.parquet")
    items_file.createOrReplaceTempView("items_file")

    monthly_average = spark.sql(
        """
        SELECT AVG(total) FROM 
        (
            SELECT sum(double(price)) total FROM items_file 
            WHERE date(date) >= date('{0}-01-01') AND date(date) < date('{1}-01-01')
            GROUP BY month(date(date))
        )
        """.format(year, year+1)
    )

    return monthly_average


def _get_monthly_percentage_change(year: int):
    items_file = spark.read.parquet("output/items.parquet")
    items_file.createOrReplaceTempView("items_file")

    monthly_change = spark.sql(
        """
        SELECT
            product_name,
            ((cnt_february - cnt_january)/cnt_january)*100 AS February,
            ((cnt_march - cnt_february)/cnt_february)*100 AS March,
            ((cnt_april - cnt_march)/cnt_march)*100 AS April,
            ((cnt_may - cnt_april)/cnt_april)*100 AS May,
            ((cnt_june - cnt_may)/cnt_may)*100 AS June,
            ((cnt_july - cnt_june)/cnt_june)*100 AS July,
            ((cnt_august - cnt_july)/cnt_july)*100 AS August,
            ((cnt_september - cnt_august)/cnt_august)*100 AS September,
            ((cnt_october - cnt_september)/cnt_september)*100 AS October,
            ((cnt_november - cnt_october)/cnt_october)*100 AS November,
            ((cnt_december - cnt_november)/cnt_november)*100 AS December
        FROM
        (
            SELECT
                product_name,
                SUM(CASE date_part('month',date(date)) WHEN 1 THEN 1 ELSE 0 END) AS cnt_january,
                SUM(CASE date_part('month',date(date)) WHEN 2 THEN 1 ELSE 0 END) AS cnt_february,
                SUM(CASE date_part('month',date(date)) WHEN 3 THEN 1 ELSE 0 END) AS cnt_march,
                SUM(CASE date_part('month',date(date)) WHEN 4 THEN 1 ELSE 0 END) AS cnt_april,
                SUM(CASE date_part('month',date(date)) WHEN 5 THEN 1 ELSE 0 END) AS cnt_may,
                SUM(CASE date_part('month',date(date)) WHEN 6 THEN 1 ELSE 0 END) AS cnt_june,
                SUM(CASE date_part('month',date(date)) WHEN 7 THEN 1 ELSE 0 END) AS cnt_july,
                SUM(CASE date_part('month',date(date)) WHEN 8 THEN 1 ELSE 0 END) AS cnt_august,
                SUM(CASE date_part('month',date(date)) WHEN 9 THEN 1 ELSE 0 END) AS cnt_september,
                SUM(CASE date_part('month',date(date)) WHEN 10 THEN 1 ELSE 0 END) AS cnt_october,
                SUM(CASE date_part('month',date(date)) WHEN 11 THEN 1 ELSE 0 END) AS cnt_november,
                SUM(CASE date_part('month',date(date)) WHEN 12 THEN 1 ELSE 0 END) AS cnt_december
            FROM items_file
            WHERE 1=1
                AND date(date) >= date('{0}-01-01') 
                AND date(date) < date('{1}-01-01') 
                AND purchased == true
            GROUP BY product_name
        )
        ORDER BY product_name
        """.format(year, year+1)
    )

    return monthly_change