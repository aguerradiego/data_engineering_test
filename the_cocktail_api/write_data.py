import json

import jsonschema
from pyspark import SparkContext
from pyspark.shell import spark
from pyspark.sql.types import StructType, StructField, StringType, DoubleType, BooleanType, IntegerType

sc = SparkContext.getOrCreate()

schema = {
  "type": "object",
  "properties": {
    "user_id": {"type": "integer"},
    "date": {"type": "string"},
    "product_name": {"type": "string"},
    "price": {"type": "number"},
    "purchased": {"type": "boolean"}
  },
  "required": [
    "user_id",
    "date",
    "product_name",
    "price",
    "purchased"
  ]
}

df_schema = StructType([
    StructField("user_id", IntegerType(), False),
    StructField("date", StringType(), False),
    StructField("product_name", StringType(), False),
    StructField("price", DoubleType(), False),
    StructField("purchased", BooleanType(), False)
])


def check_json_schema(json_content, json_schema):
    try:
        jsonschema.validate(instance=json_content, schema=json_schema)
    except jsonschema.exceptions.ValidationError as err:
        return False
    return True


def write_item_to_parquet(json_content):
    content = [json.dumps(json_content)]
    df = spark.read.json(sc.parallelize(content), schema=df_schema)
    df.write.mode('append').parquet("output/items.parquet")
